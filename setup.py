from setuptools import setup, find_packages


def main():
    console_scripts = [
        "train_kirsh = kirsh.train:run",
        "train_kirsh_ellipse = kirsh_ellipse.train:run",
        "train_kirshISI = kirshISI.train_kirshISI:main",
        "train_annular_ring = annular_ring.train_annular_ring:main",
        "post_process_kirsh = kirsh.post_process_kirsh:main",
        "post_process_kirshISI = kirshISI.post_process_kirshISI:main"
    ]
    setup(
        name='simnet_wrapper',
        version='0.1.0',
        package_dir={"": "src"},
        packages=find_packages("src"),
        description='diploma work',
        entry_points={
            "console_scripts": console_scripts
        }
    )


if __name__ == "__main__":
    main()
