import os

import numpy as np
import matplotlib.pyplot as plt

from modulus.data import BC
from modulus.variables import Variables

from total_properties import AnnularRingProperties as pr


class AnnularRingISI(BC):
    def __init__(self, grid_res, sdf_fn, velocity_grad_fn):
        self.batch_size = 4000
        self.importance_prob_grid = None
        self.update_importance_prob_freq = 200
        self.sampled_counter = 0
        self.importance_prob_warmup = 200
        self.epsilon_uniform = 0.

        # custom variables
        self.grid_res = grid_res
        self.sdf_fn = sdf_fn
        self.velocity_grad_fn = velocity_grad_fn

    def generate_importance_prob_grid(self):
        # sample 10 times as many points in batch to do adaptive sampling
        mesh_x, mesh_y = np.meshgrid(np.linspace(pr.channel_length[0], pr.channel_length[1], self.grid_res[0]),
                                     np.linspace(-pr.outer_cylinder_radius, pr.outer_cylinder_radius,
                                                 self.grid_res[1]),
                                     indexing='ij')
        mesh_x = np.reshape(mesh_x, (self.grid_res[0] * self.grid_res[1], 1))
        mesh_y = np.reshape(mesh_y, (self.grid_res[0] * self.grid_res[1], 1))

        # compute sdf
        sdf = self.sdf_fn(x=mesh_x, y=mesh_y)

        # compute velocity grad
        velocity_grad = self.velocity_grad_fn(Variables({'x': mesh_x, 'y': mesh_y}))

        # compute probability density (zero if sdf is negative/outside domain)
        # probabiliy equal to the norm of the gradient
        dx = (pr.channel_length[1] - pr.channel_length[0]) / (self.grid_res[0] - 1)
        dy = (2 * pr.outer_cylinder_radius) / (self.grid_res[1] - 1)
        element_size = dx * dy
        vel_grad_norm = (velocity_grad['u__x'] ** 2
                         + velocity_grad['u__y'] ** 2
                         + velocity_grad['v__x'] ** 2
                         + velocity_grad['v__y'] ** 2) ** 0.5
        if self.sampled_counter < self.importance_prob_warmup:
            unnormalized_prob = np.heaviside(sdf + max(dx, dy), 0.0) * (
                np.ones_like(sdf))  # uniform distribution
        else:
            unnormalized_prob = np.heaviside(sdf + max(dx, dy), 0.0) * (vel_grad_norm + self.epsilon_uniform)
        unnormalized_prob = np.reshape(unnormalized_prob, self.grid_res)

        # normalize probability distribution, (||prob|| = 1)
        area = (pr.channel_length[1] - pr.channel_length[0]) * (2 * pr.outer_cylinder_radius)
        normalized_prob = unnormalized_prob / np.sum(area * unnormalized_prob / (self.grid_res[0] * self.grid_res[1]))
        # normalized_prob = unnormalized_prob / np.sum(unnormalized_prob)
        print(np.sum(unnormalized_prob), np.sum(normalized_prob), area / (self.grid_res[0] * self.grid_res[1]))

        # plot function for viewing
        path = 'images'
        i = 0 if len(os.listdir(path)) == 0 else max(
            [int(val.split('_')[-1].split('.')[0]) for val in os.listdir(path)]) + 1

        plt.imshow(normalized_prob)
        plt.title("Sample Probability")
        plt.colorbar()
        plt.savefig(f"images/sample_prob_{i}.png")
        plt.close()

        return normalized_prob

    def invar_names(self):
        return ['x', 'y', 'area']

    def outvar_names(self):
        return ['continuity', 'momentum_x', 'momentum_y']

    def lambda_names(self):
        return ['lambda_continuity', 'lambda_momentum_x', 'lambda_momentum_y']

    def invar_fn(self, batch_size):
        # generate updated probability
        if self.sampled_counter == 0 or (
                self.sampled_counter % self.update_importance_prob_freq == 0 and self.sampled_counter > self.importance_prob_warmup):
            self.importance_prob_grid = self.generate_importance_prob_grid()
        self.sampled_counter += 1

        # sample points
        invar = {'x': np.zeros((0, 1)),
                 'y': np.zeros((0, 1)),
                 'area': np.zeros((0, 1))}
        while True:
            # sample size (try to sample more then the batch so only do 1 loop)
            sample_size = 4 * batch_size

            # sample points in range
            x = np.random.uniform(pr.channel_length[0], pr.channel_length[1], [sample_size, 1])
            y = np.random.uniform(-pr.outer_cylinder_radius, pr.outer_cylinder_radius, [sample_size, 1])
            rand = np.random.uniform(0, np.max(self.importance_prob_grid), [sample_size, 1])

            # compute index for interpolation
            dx = (pr.channel_length[1] - pr.channel_length[0]) / (self.grid_res[0] - 1)
            dy = (2 * pr.outer_cylinder_radius) / (self.grid_res[1] - 1)
            x_index = np.round((x - pr.channel_length[0]) / dx).astype(dtype=np.int)
            y_index = np.round((y + pr.outer_cylinder_radius) / dy).astype(dtype=np.int)

            # index importance grid
            prob = self.importance_prob_grid[x_index[:, 0], y_index[:, 0]]

            # remove points according to the rejection method
            # (https://web.mit.edu/urban_or_book/www/book/chapter7/7.1.3.html)
            # also remove any point on edges with negative sdf
            sdf = self.sdf_fn(x=x, y=y)
            remove_criteria = np.logical_and(rand[:, 0] < prob, sdf[:, 0] > 0)
            x = x[remove_criteria, :]
            y = y[remove_criteria, :]
            prob = np.expand_dims(prob[remove_criteria], axis=-1)

            # add points to out dictionary
            invar['x'] = np.concatenate([invar['x'], x], axis=0)
            invar['y'] = np.concatenate([invar['y'], y], axis=0)
            invar['area'] = np.concatenate([invar['area'], prob], axis=0)

            # check if sampled enough points
            if invar['x'].shape[0] >= batch_size:
                invar['x'] = invar['x'][:batch_size]
                invar['y'] = invar['y'][:batch_size]
                invar['area'] = 1.0 / (invar['area'][:batch_size] * batch_size)
                break

        return invar

    def outvar_fn(self, invar):
        outvar = {'continuity': np.zeros_like(invar['x']),
                  'momentum_x': np.zeros_like(invar['x']),
                  'momentum_y': np.zeros_like(invar['x'])}
        return outvar

    def lambda_fn(self, invar, outvar):
        lambda_weighting = {'lambda_continuity': self.sdf_fn(x=invar['x'], y=invar['y']) * invar['area'],
                            'lambda_momentum_x': self.sdf_fn(x=invar['x'], y=invar['y']) * invar['area'],
                            'lambda_momentum_y': self.sdf_fn(x=invar['x'], y=invar['y']) * invar['area']}
        return lambda_weighting
