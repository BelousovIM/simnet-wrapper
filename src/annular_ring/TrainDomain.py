from sympy import Eq

from modulus.dataset import TrainDomain
from modulus.sympy_utils.functions import parabola
from modulus.sympy_utils.numpy_printer import np_lambdify
from modulus.variables import Variables, Key
from modulus.graph import unroll_graph

from total_properties import AnnularRingProperties as pr
from annular_ring.ImportanceSampledInterior import AnnularRingISI as importance_sampler


class AnnularRingTrain(TrainDomain):
    def __init__(self, **config):
        super(AnnularRingTrain, self).__init__(nr_threads=1)

        # inlet
        inlet_sympy = parabola(pr.y, inter_1=pr.channel_width[0], inter_2=pr.channel_width[1], height=pr.inlet_vel)
        inlet = pr.geo.boundary_bc(outvar_sympy={'u': inlet_sympy, 'v': 0},
                                   batch_size_per_area=32,
                                   criteria=Eq(pr.x, pr.channel_length[0]))
        self.add(inlet, name="Inlet")

        # outlet
        outlet = pr.geo.boundary_bc(outvar_sympy={'p': 0},
                                    batch_size_per_area=32,
                                    criteria=Eq(pr.x, pr.channel_length[1]))
        self.add(outlet, name="Outlet")

        # noslip
        noslip = pr.geo.boundary_bc(outvar_sympy={'u': 0, 'v': 0},
                                    batch_size_per_area=32,
                                    criteria=(pr.x > pr.channel_length[0]) & (pr.x < pr.channel_length[1]))
        self.add(noslip, name="NoSlip")

        # importance sampled interior
        # evaluate flow on grid for generating importance sampling probability function
        grid_res = [1024, 512]

        # make numpy function for computing the sdf
        sdf_fn = np_lambdify(pr.geo.sdf,
                             ['x', 'y'])

        # make function for computing the network velocity derivatives
        # train domain now has access to sess, nets, and equations
        invar_placeholder = Variables.tf_variables(['x', 'y'],
                                                   batch_size=grid_res[0] * grid_res[1])
        outvar_velocity_grad = unroll_graph(self.nets + self.equations,
                                            invar_placeholder,
                                            Key.convert_list(['u__x', 'u__y', 'v__x', 'v__y']))
        velocity_grad_fn = Variables.lambdify_np(invar_placeholder,
                                                 outvar_velocity_grad, self.sess)

        # interior
        interior = importance_sampler(grid_res=grid_res, sdf_fn=sdf_fn, velocity_grad_fn=velocity_grad_fn)
        self.add(interior, name="Interior")

        # make integral continuity
        outlet = pr.geo.boundary_bc(outvar_sympy={'integral_continuity': 2},
                                    batch_size_per_area=128,
                                    lambda_sympy={'lambda_integral_continuity': 0.1},
                                    criteria=Eq(pr.x, pr.channel_length[1]))
        self.add(outlet, name="OutletContinuity")
