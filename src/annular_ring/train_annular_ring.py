import tensorflow as tf

from modulus import solver, controller
from modulus.dataset import ValidationDomain, InferenceDomain, MonitorDomain
from modulus.data import Validation, Monitor, Inference
from modulus.PDES.navier_stokes import IntegralContinuity, NavierStokes

from annular_ring.TrainDomain import AnnularRingTrain
from total_properties import AnnularRingProperties as pr


class AnnularRingInference(InferenceDomain):
    def __init__(self, **config):
        super(AnnularRingInference, self).__init__()
        # save entire domain
        interior = Inference(pr.openfoam_invar_numpy, ['u', 'v', 'p'])
        self.add(interior, name="Inference")


class AnnularRingVal(ValidationDomain):
    def __init__(self, **config):
        super(AnnularRingVal, self).__init__()
        val = Validation.from_numpy(pr.openfoam_invar_numpy, pr.openfoam_outvar_numpy)
        self.add(val, name='Val')


class AnnularRingMonitor(MonitorDomain):
    def __init__(self, **config):
        super(AnnularRingMonitor, self).__init__()
        # metric for pressure drop and mass imbalance
        global_monitor = Monitor(
            pr.geo.sample_interior(512, bounds={pr.x: pr.channel_length, pr.y: (-pr.outer_cylinder_radius, pr.outer_cylinder_radius)}),
            {'mass_imbalance': lambda var: tf.reduce_sum(var['area'] * tf.abs(var['continuity'])),
             'momentum_imbalance': lambda var: tf.reduce_sum(
                 var['area'] * tf.abs(var['momentum_x']) + tf.abs(var['momentum_x']))})
        self.add(global_monitor, 'GlobalMonitor')

        # metric for force on inner sphere
        force = Monitor(pr.inner_circle.sample_boundary(512),
                        {'force_x': lambda var: tf.reduce_sum(var['normal_x'] * var['area'] * var['p']),
                         'force_y': lambda var: tf.reduce_sum(var['normal_y'] * var['area'] * var['p'])})
        self.add(force, 'Force')


class ChipSolver(solver.Solver):
    train_domain = AnnularRingTrain
    val_domain = AnnularRingVal
    inference_domain = AnnularRingInference
    monitor_domain = AnnularRingMonitor

    def __init__(self, **config):
        super(ChipSolver, self).__init__(**config)
        self.equations = (NavierStokes(nu=0.01, rho=1, dim=2, time=False).make_node()
                          + IntegralContinuity(dim=2).make_node())
        flow_net = self.arch.make_node(name='flow_net',
                                       inputs=['x', 'y'],
                                       outputs=['u', 'v', 'p'])
        self.nets = [flow_net]

    @classmethod
    def update_defaults(cls, defaults):
        defaults.update({
            'network_dir': './checkpoints/network_checkpoint_annal_ring',
            'decay_steps': 2000,
            'max_steps': 200000,
        })


def main():
    ctr = controller.ModulusController(ChipSolver)
    ctr.run()


if __name__ == '__main__':
    main()
