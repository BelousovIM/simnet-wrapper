from sympy import Eq

from modulus.dataset import TrainDomain
from modulus.sympy_utils.numpy_printer import np_lambdify
from modulus.variables import Variables, Key
from modulus.graph import unroll_graph

from total_properties import KirshProperties as pr
from kirshISI.ImportanceSampledInterior import KirshISI as importance_sampler


class KirshTrain(TrainDomain):
    def __init__(self, **config):
        super(KirshTrain, self).__init__()

        square_bottom_top = pr.geo.boundary_bc(outvar_sympy={'traction_x': 0., 'traction_y': 0.},
                                               batch_size_per_area=128,
                                               criteria=Eq(pr.y, pr.square_origin[1]) | Eq(pr.y, pr.square_origin[1] +
                                                                                           pr.square_dim[1]),
                                               fixed_var=True)
        self.add(square_bottom_top, name="square_bottom_top")

        square_left = pr.geo.boundary_bc(outvar_sympy={'traction_x': -pr.T, 'traction_y': 0.},
                                         batch_size_per_area=300,
                                         criteria=Eq(pr.x, pr.square_origin[0]),
                                         fixed_var=True)
        self.add(square_left, name="square_left")

        square_right = pr.geo.boundary_bc(outvar_sympy={'traction_x': pr.T, 'traction_y': 0.},
                                          batch_size_per_area=300,
                                          criteria=Eq(pr.x, pr.square_origin[0] + pr.square_dim[0]),
                                          fixed_var=True)
        self.add(square_right, name="square_right")

        square_circle = pr.circle.boundary_bc(outvar_sympy={'traction_x': 0., 'traction_y': 0.},
                                              batch_size_per_area=1000,
                                              fixed_var=True)
        self.add(square_circle, name="square_circle")

        # importance sampled interior
        # evaluate flow on grid for generating importance sampling probability function
        grid_res = [512, 512]

        # make numpy function for computing the sdf
        sdf_fn = np_lambdify(pr.geo.sdf,
                             ['x', 'y'])

        # make function for computing the network velocity derivatives
        # train domain now has access to sess, nets, and equations
        invar_placeholder = Variables.tf_variables(['x', 'y'],
                                                   batch_size=grid_res[0] * grid_res[1])
        outvar_velocity_grad = unroll_graph(self.nets + self.equations,
                                            invar_placeholder,
                                            Key.convert_list(['sigma_xx__x', 'sigma_xx__y',
                                                              'sigma_yy__x', 'sigma_yy__y',
                                                              'sigma_xy__x', 'sigma_xy__y']))
        velocity_grad_fn = Variables.lambdify_np(invar_placeholder,
                                                 outvar_velocity_grad, self.sess)

        # interior
        interior = importance_sampler(grid_res=grid_res, sdf_fn=sdf_fn, velocity_grad_fn=velocity_grad_fn)
        self.add(interior, name="Interior")
