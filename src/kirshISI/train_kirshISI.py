from modulus.dataset import ValidationDomain
from modulus.data import Validation
from modulus.csv_utils.csv_rw import csv_to_dict
from modulus import solver, controller
from modulus.PDES.linear_elasticity import LinearElasticityPlaneStress

from total_properties import KirshProperties as pr
from kirsh_ex.TrainDomain import KirshTrain


class KirshVal(ValidationDomain):
    def __init__(self, **config):
        super(KirshVal, self).__init__()
        # validation data
        KirshSol = csv_to_dict('KirshSol/KirshSol.csv')

        KirshSol_invar = {key: value for key, value in KirshSol.items() if key in ['x', 'y']}
        KirshSol_outvar = {key: value for key, value in KirshSol.items() if key in ['sigma_xx', 'sigma_yy', 'sigma_xy']}

        val = Validation.from_numpy(KirshSol_invar, KirshSol_outvar)
        self.add(val, name='KirshVal')


class KirshISISolver(solver.Solver):
    train_domain = KirshTrain
    val_domain = KirshVal

    def __init__(self, **config):
        super(KirshISISolver, self).__init__(**config)
        self.equations = (LinearElasticityPlaneStress(lambda_=pr.lambda_, mu=pr.mu).make_node())
        elasticity_net = self.arch.make_node(name='elasticity_net',
                                             inputs=['x', 'y'],
                                             outputs=['u', 'v', 'sigma_xx', 'sigma_yy', 'sigma_xy'])
        self.nets = [elasticity_net]

    @classmethod
    def update_defaults(cls, defaults):
        defaults.update({
            'network_dir': './checkpoints/network_checkpoint_kirsh_AS_2',
            'rec_results_cpu': True,
            'max_steps': 500_000,
            'decay_steps': 5000,
            'start_lr': 1e-3,
            'nr_layers': 6
        })


def main():
    ctr = controller.ModulusController(KirshISISolver)
    ctr.run()


if __name__ == '__main__':
    main()
