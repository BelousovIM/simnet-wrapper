import warnings

import modulus
import numpy as np
import torch
from modulus.PDES.linear_elasticity import LinearElasticityPlaneStress
from modulus.continuous.constraints.constraint import (
    PointwiseBoundaryConstraint,
    PointwiseInteriorConstraint,
)
from modulus.continuous.domain.domain import Domain
from modulus.continuous.inferencer.inferencer import PointwiseInferencer
from modulus.continuous.solvers.solver import Solver
from modulus.continuous.validator.validator import PointwiseValidator
from modulus.csv_utils.csv_rw import csv_to_dict
from modulus.hydra import to_absolute_path, to_yaml, instantiate_arch
from modulus.hydra.config import ModulusConfig
from modulus.key import Key
from sympy import Eq

from total_properties import KirshEllipseProperties as Properties

device = "cuda:0" if torch.cuda.is_available() else "cpu"
warnings.filterwarnings("ignore")


@modulus.main(config_path="conf", config_name="config_fc")
def run(cfg: ModulusConfig) -> None:
    print(to_yaml(cfg))
    pr = Properties()

    le = LinearElasticityPlaneStress(lambda_=pr.lambda_, mu=pr.mu)

    total_net = instantiate_arch(
        input_keys=[Key("x"), Key("y"), Key("a"), Key("phi")],
        output_keys=[
            Key("u"), Key("v"),
            Key("sigma_xx"),
            Key("sigma_yy"),
            Key("sigma_xy"),
        ],
        cfg=cfg.arch.fully_connected,
    )
    nodes = (
        le.make_nodes()
        + [total_net.make_node(name="stress_network", jit=cfg.jit)]
    )

    ##########################################################################################################
    domain = Domain()

    square_bottom_top = PointwiseBoundaryConstraint(
        nodes=nodes,
        geometry=pr.geo,
        outvar={"traction_x": 0., "traction_y": 0.},
        batch_size=cfg.batch_size.square_bottom_top,
        criteria=Eq(pr.y, pr.square_origin[1]) | Eq(pr.y, pr.square_origin[1] + pr.square_dim[1]),
        param_ranges=pr.param_ranges,
    )
    domain.add_constraint(square_bottom_top, "square_bottom_top")

    square_left = PointwiseBoundaryConstraint(
        nodes=nodes,
        geometry=pr.geo,
        outvar={"traction_x": -pr.T, "traction_y": 0.},
        batch_size=cfg.batch_size.square_left,
        criteria=Eq(pr.x, pr.square_origin[0]),
        param_ranges=pr.param_ranges,
    )
    domain.add_constraint(square_left, name="square_left")

    square_right = PointwiseBoundaryConstraint(
        nodes=nodes,
        geometry=pr.geo,
        outvar={"traction_x": pr.T, "traction_y": 0.},
        batch_size=cfg.batch_size.square_right,
        criteria=Eq(pr.x, pr.square_origin[0] + pr.square_dim[0]),
        param_ranges=pr.param_ranges,
    )
    domain.add_constraint(square_right, name="square_right")

    square_circle = PointwiseBoundaryConstraint(
        nodes=nodes,
        geometry=pr.ellipse,
        outvar={"traction_x": 0., "traction_y": 0.},
        batch_size=cfg.batch_size.square_circle,
        param_ranges=pr.param_ranges,
    )
    domain.add_constraint(square_circle, name="square_circle")

    # linear elasticity
    interior = PointwiseInteriorConstraint(
        nodes=nodes,
        geometry=pr.geo,
        outvar={
            'equilibrium_x': 0.,
            'equilibrium_y': 0.,
            'stress_disp_xx': 0.,
            'stress_disp_yy': 0.,
            'stress_disp_xy': 0.
        },
        lambda_weighting={
            'equilibrium_x': pr.geo.sdf,
            'equilibrium_y': pr.geo.sdf,
            'stress_disp_xx': pr.geo.sdf,
            'stress_disp_yy': pr.geo.sdf,
            'stress_disp_xy': pr.geo.sdf
        },
        bounds={pr.x: pr.bounds_x, pr.y: pr.bounds_y},
        batch_size=cfg.batch_size.interior,
        param_ranges=pr.param_ranges,
    )
    domain.add_constraint(interior, name="Interior")

    ##########################################################################################################
    KirshSol = csv_to_dict(to_absolute_path("KirshSol/KirshSol.csv"))

    KirshSol_invar = {key: value for key, value in KirshSol.items() if key in ['x', 'y']}
    KirshSol_invar["a"] = np.ones_like(KirshSol_invar["x"]) * pr.b
    KirshSol_invar["phi"] = np.zeros_like(KirshSol_invar["x"])

    KirshSol_outvar = {key: value for key, value in KirshSol.items() if key in ['sigma_xx', 'sigma_yy', 'sigma_xy']}

    validator = PointwiseValidator(
        KirshSol_invar, KirshSol_outvar, nodes, batch_size=128
    )
    domain.add_validator(validator)

    ##########################################################################################################
    grid_inference = PointwiseInferencer(
        KirshSol_invar,
        [
            "u",
            "v",
            "sigma_xx",
            "sigma_yy",
            "sigma_xy",
        ],
        nodes,
        batch_size=128,
    )
    domain.add_inferencer(grid_inference, "inf_data")

    ##########################################################################################################
    slv = Solver(cfg, domain)

    # start solver
    slv.solve()


if __name__ == '__main__':
    run()
