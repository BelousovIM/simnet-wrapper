from sympy import Eq

from modulus.dataset import TrainDomain

from total_properties import KirshParametricProperties as pr


class KirshTrain(TrainDomain):
    def __init__(self, **config):
        super(KirshTrain, self).__init__()

        square_bottom_top = pr.geo.boundary_bc(outvar_sympy={'traction_x': 0., 'traction_y': 0.},
                                               batch_size_per_area=128,
                                               criteria=Eq(pr.y, pr.square_origin[1]) | Eq(pr.y, pr.square_origin[1] +
                                                                                           pr.square_dim[1]),
                                               param_ranges=pr.param_ranges,
                                               fixed_var=True)
        self.add(square_bottom_top, name="square_bottom_top")

        square_left = pr.geo.boundary_bc(outvar_sympy={'traction_x': -pr.T, 'traction_y': 0.},
                                         batch_size_per_area=300,
                                         criteria=Eq(pr.x, pr.square_origin[0]),
                                         param_ranges=pr.param_ranges,
                                         fixed_var=True)
        self.add(square_left, name="square_left")

        square_right = pr.geo.boundary_bc(outvar_sympy={'traction_x': pr.T, 'traction_y': 0.},
                                          batch_size_per_area=300,
                                          criteria=Eq(pr.x, pr.square_origin[0] + pr.square_dim[0]),
                                          param_ranges=pr.param_ranges,
                                          fixed_var=True)
        self.add(square_right, name="square_right")

        square_circle = pr.circle.boundary_bc(outvar_sympy={'traction_x': 0., 'traction_y': 0.},
                                              batch_size_per_area=1000,
                                              param_ranges=pr.param_ranges,
                                              fixed_var=True)
        self.add(square_circle, name="square_circle")

        # linear elasticity
        cond = pr.x ** 2 + pr.y ** 2 > (pr.circle_radius + pr.eps) ** 2
        interior = pr.geo.interior_bc(outvar_sympy={'equilibrium_x': 0.,
                                                    'equilibrium_y': 0.,
                                                    'stress_disp_xx': 0.,
                                                    'stress_disp_yy': 0.,
                                                    'stress_disp_xy': 0.},
                                      lambda_sympy={'lambda_equilibrium_x': pr.geo.sdf,
                                                    'lambda_equilibrium_y': pr.geo.sdf,
                                                    'lambda_stress_disp_xx': pr.geo.sdf,
                                                    'lambda_stress_disp_yy': pr.geo.sdf,
                                                    'lambda_stress_disp_xy': pr.geo.sdf},
                                      bounds={pr.x: pr.bounds_x, pr.y: pr.bounds_y},
                                      batch_size_per_area=4000,
                                      criteria=cond,
                                      param_ranges=pr.param_ranges,
                                      fixed_var=True)
        self.add(interior, name="Interior")

        interior1 = pr.geo.interior_bc(outvar_sympy={'equilibrium_x': 0.,
                                                     'equilibrium_y': 0.,
                                                     'stress_disp_xx': 0.,
                                                     'stress_disp_yy': 0.,
                                                     'stress_disp_xy': 0.},
                                       lambda_sympy={'lambda_equilibrium_x': pr.geo.sdf,
                                                     'lambda_equilibrium_y': pr.geo.sdf,
                                                     'lambda_stress_disp_xx': pr.geo.sdf,
                                                     'lambda_stress_disp_yy': pr.geo.sdf,
                                                     'lambda_stress_disp_xy': pr.geo.sdf},
                                       bounds={pr.x: pr.bounds_x, pr.y: pr.bounds_y},
                                       batch_size_per_area=45000,
                                       criteria=~cond,
                                       param_ranges=pr.param_ranges,
                                       fixed_var=True)
        self.add(interior1, name="Interior1")
