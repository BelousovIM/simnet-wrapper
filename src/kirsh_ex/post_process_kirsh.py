import warnings

import numpy as np
import pandas as pd

from modulus.dataset import ValidationDomain
from modulus.data import Validation
from modulus import solver, controller
from modulus.PDES.linear_elasticity import LinearElasticityPlaneStress

from utils import getKrshStressT
from total_properties import KirshParametricProperties as pr
from kirsh_ex.TrainDomain import KirshTrain

warnings.filterwarnings('ignore')


class KirshVal(ValidationDomain):
    def __init__(self, **config):
        super(KirshVal, self).__init__()

        radius = 0.08
        # f_param = 0.08

        # example1
        x = np.linspace(pr.circle_center[0] + radius,
                        pr.square_origin[0] + pr.square_dim[0],
                        pr.num_of_points).reshape(-1, 1)
        y = np.zeros_like(x)

        # example2
        # x, y = y, x # вдоль вертикальной оси

        # example3
        # matrix = (np.ones((num_of_points, 1)) @ np.linspace(-0.5, 0.5, num=num_of_points).reshape(1, -1))
        # x = matrix.ravel()
        # y = matrix.T.ravel()
        # ind = x**2 + y**2 >= radius**2
        # x = x[ind]
        # y = y[ind]
        # x, y = x.reshape(-1, 1), y.reshape(-1, 1)

        # example4
        # y = pd.read_csv(f'fidesys_val/xx_{f_param}.csv')['Длина линии'].values.reshape(-1, 1) + radius
        # x = np.zeros_like(y)

        stress = [getKrshStressT(val, radius, pr.T) for val in zip(x.ravel(), y.ravel())]
        sigma_xx = [val[0] for val in stress]
        sigma_yy = [val[1] for val in stress]
        sigma_xy = [val[2] for val in stress]
        sigma_xx, sigma_yy, sigma_xy = np.array(sigma_xx), np.array(sigma_yy), np.array(sigma_xy)
        sigma_xx, sigma_yy, sigma_xy = sigma_xx.reshape(-1, 1), sigma_yy.reshape(-1, 1), sigma_xy.reshape(-1, 1)
        KirshSol_invar = {'x': x, 'y': y, 'circle_radius': np.ones_like(x) * radius}
        KirshSol_outvar = {'sigma_xx': sigma_xx, 'sigma_yy': sigma_yy, 'sigma_xy': sigma_xy}

        val = Validation.from_numpy(KirshSol_invar, KirshSol_outvar)
        self.add(val, name='KirshVal')


class KirshSolver(solver.Solver):
    train_domain = KirshTrain
    val_domain = KirshVal

    def __init__(self, **config):
        super(KirshSolver, self).__init__(**config)
        self.equations = (LinearElasticityPlaneStress(lambda_=pr.lambda_, mu=pr.mu).make_node())
        elasticity_net = self.arch.make_node(name='elasticity_net',
                                             inputs=['x', 'y', 'circle_radius'],
                                             outputs=['u', 'v', 'sigma_xx', 'sigma_yy', 'sigma_xy'])
        self.nets = [elasticity_net]

    @classmethod
    def update_defaults(cls, defaults):
        defaults.update({
            'network_dir': './checkpoints/network_checkpoint_kirsh_1',
        })


def main():
    ctr = controller.ModulusController(KirshSolver)
    ctr.run()


if __name__ == '__main__':
    main()
