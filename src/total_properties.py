from dataclasses import dataclass

from sympy import Symbol
import numpy as np

from modulus.geometry.csg.csg_2d import Rectangle, Circle, Ellipse
from modulus.csv_utils.csv_rw import csv_to_dict


@dataclass
class KirshProperties:
    # Panel properties
    E = 73.0 * 10 ** 9  # Pa
    nu = 0.33
    lambda_ = nu * E / ((1 + nu) * (1 - 2 * nu))  # Pa
    mu_real = E / (2 * (1 + nu))  # Pa
    lambda_ = lambda_ / mu_real  # Dimensionless
    mu = 1.  # Dimensionless
    T = 9e3

    # Geometry
    circle_radius = 0.08
    circle_center = (0, 0)
    eps = 0.05
    square_origin = (-1., -1.)
    square_dim = (2, 2)
    hr_square_origin = (-0.15, -0.15)
    hr_square_dim = (2 * abs(hr_square_origin[0]), 2 * abs(hr_square_origin[1]))

    square = Rectangle(square_origin,
                       (square_origin[0] + square_dim[0],
                        square_origin[1] + square_dim[1]))

    # Bounds
    x, y = Symbol("x"), Symbol("y")
    bounds_x = (square_origin[0], square_origin[0] + square_dim[0])
    bounds_y = (square_origin[1], square_origin[1] + square_dim[1])
    hr_bounds_x = (hr_square_origin[0], hr_square_origin[0] + hr_square_dim[0])
    hr_bounds_y = (hr_square_origin[1], hr_square_origin[1] + hr_square_dim[1])

    def __post_init__(self):
        self.circle = Circle(self.circle_center, self.circle_radius)
        self.geo = self.square - self.circle
        hr_square = Rectangle(
            self.hr_square_origin,
            (
                self.hr_square_origin[0] + self.hr_square_dim[0],
                self.hr_square_origin[1] + self.hr_square_dim[1]
            ),
        )
        self.hr_geo = self.geo & hr_square


@dataclass
class KirshParametricProperties(KirshProperties):

    # parametric variation
    circle_radius = Symbol("circle_radius")
    circle_radius_range = (0.08, 0.1)
    param_ranges = {circle_radius: circle_radius_range}


@dataclass
class KirshEllipseProperties(KirshProperties):

    # parametric variation
    phi = Symbol("phi")
    a = Symbol("a")
    a_range = (0.08, 0.1)
    phi_range = (0., np.pi / 2)
    param_ranges = {phi: phi_range, a: a_range}

    # Geometry
    b = 0.08

    def __post_init__(self):
        self.ellipse = Ellipse(self.circle_center, self.a, self.b)
        self.ellipse.rotate(self.phi)
        self.geo = self.square - self.ellipse


@dataclass
class AnnularRingProperties:
    # params for domain
    channel_length = (-6.732, 6.732)
    channel_width = (-1.0, 1.0)
    cylinder_center = (0.0, 0.0)
    outer_cylinder_radius = 2.0
    inner_cylinder_radius = 1.0
    inlet_vel = 1.5

    # define geometry
    rec = Rectangle((channel_length[0], channel_width[0]),
                    (channel_length[1], channel_width[1]))
    outer_circle = Circle(cylinder_center, outer_cylinder_radius)
    inner_circle = Circle((0, 0), inner_cylinder_radius)
    geo = (rec + outer_circle) - inner_circle

    # define sympy varaibles to parametize domain curves
    x, y = Symbol('x'), Symbol('y')

    # validation data
    mapping = {'Points:0': 'x', 'Points:1': 'y',
               'U:0': 'u', 'U:1': 'v', 'p': 'p'}
    openfoam_var = csv_to_dict('examples/annular_ring/openfoam/bend_finerInternal0.csv', mapping)
    openfoam_var['x'] += channel_length[0]  # center OpenFoam data
    openfoam_invar_numpy = {key: value for key, value in openfoam_var.items() if key in ['x', 'y']}
    openfoam_outvar_numpy = {key: value for key, value in openfoam_var.items() if key in ['u', 'v', 'p']}
